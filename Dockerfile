FROM alpine:3.20
LABEL maintainer="Enmanuel Moreira"

RUN apk --no-cache add \
    sudo \
    python3\
    py3-pip \
    openssl \
    ca-certificates \
    sshpass \
    openssh-client \
    rsync \
    git && \
    apk --no-cache add --virtual build-dependencies \
    python3-dev \
    libffi-dev \
    musl-dev \
    gcc \
    cargo \
    openssl-dev \
    libressl-dev \
    build-base

RUN pip3 install --no-cache-dir --upgrade pip wheel cryptography pywinrm cffi

RUN pip3 install --no-cache-dir ansible-core ansible ansible-lint mitogen jmespath

RUN apk del build-dependencies && \
    rm -rf /var/cache/apk/* && \
    rm -rf /root/.cache/pip && \
    rm -rf /root/.cargo

RUN mkdir /ansible && \
    mkdir -p /etc/ansible && \
    echo -e '[local]\nlocalhost ansible_connection=local' > /etc/ansible/hosts

WORKDIR /ansible

CMD [ "ansible-playbook", "--version" ]
